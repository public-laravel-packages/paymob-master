<?php

namespace _34ml\PaymobMaster\DTOs\Responses;

class CustomResponse
{
    public int $code;
    public string $message;
    public array $data;
    public function __construct($code = 200, $message = "", $data = [])
    {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }
}
