<?php

namespace _34ml\PaymobMaster\DTOs\Customer;

use _34ml\PaymobMaster\DTOs\PaymobAuthFields;

class CustomerGetRequest extends PaymobAuthFields
{
    public string $userId;
}
