<?php

namespace _34ml\PaymobMaster\Exceptions;
use Exception;

class UnsupportedDataFormatException extends Exception
{
    public function __construct( $message = "", $code = 0, $previous = null)
    {
        $this->message = !empty($message) ? $message
            : "Data Format Unsupported or can't be handled";
        parent::__construct($this->message, $code, $previous);
    }
}
