<?php

namespace _34ml\PaymobMaster\DTOs\Customer;

abstract class CustomerFamilyRole
{
    const Parent = "active_parent";
    const NonParent = "child";
}
