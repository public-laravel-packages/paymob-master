<?php

namespace _34ml\PaymobMaster\Helpers;

use _34ml\PaymobMaster\DTOs\Customer\CustomerAuthFields;
use _34ml\PaymobMaster\DTOs\Responses\CustomResponse;


class CustomerHelper
{
    public static function CustomExceptionHandler($code) : CustomResponse
    {
        switch ($code) {
            case 1:
                return new CustomResponse($code,"Empty Response");

            case 400:
                return new CustomResponse($code,"Un expected error");

            case 401:
                return new CustomResponse($code,"Un expected error");

            case 403:
                return new CustomResponse($code,"Un expected error");

            case 406:
                return new CustomResponse($code,"field in the payload does not meet
                          limitations (length, datatype, etc.).");

            case 409:
                return new CustomResponse($code,"PIN is very weak please try another one.");

            case 639:
                return new CustomResponse($code,"Consumer has set PIN.");

            case 801:
                return new CustomResponse($code,"Invalid HMAC");

            case 810:
                return new CustomResponse($code,"Customer Data Duplicate Email");

            case 811:
                return new CustomResponse($code,"Customer Data Duplicate Badge");

            case 812:
                return new CustomResponse($code,"Customer Data Duplicate User ID");

            case 1051:
                return new CustomResponse($code,"Consumer Not Defined");

            case 6051:
                return new CustomResponse($code,"Insufficient Funds, Please recharge wallet and try again.");

            default:
                return new CustomResponse($code,"Un expected error");

        }
    }

    public static function ParseAuthResponse(string $json) : CustomResponse
    {
       $data = json_decode($json);

        if(empty($data))
            return CustomerHelper::CustomExceptionHandler(1);

       $obj =  new CustomResponse();
       $obj->code = 200;
       $obj->data["accessToken"] = $data->access_token ?? "";
       $obj->data["refreshToken"] = $data->refresh_token ?? "";
       return $obj;
    }

    public static function ParseCreateResponse(string $json) : CustomResponse
    {
        $data = json_decode($json);

        if(empty($data))
            return CustomerHelper::CustomExceptionHandler(1);

        if((int) $data->TXNSTATUS != 200)
        {
            return CustomerHelper::CustomExceptionHandler((int) $data->TXNSTATUS);
        }

        return new CustomResponse();
    }

    public static function ParseGetResponse(string $json) : CustomResponse
    {
        $data = json_decode($json);

        if(empty($data))
            return CustomerHelper::CustomExceptionHandler(1);

        if((int) $data->TXNSTATUS != 200)
        {
           return CustomerHelper::CustomExceptionHandler((int) $data->TXNSTATUS);
        }

        $obj =  new CustomResponse();
        $obj->code = 200;
        $obj->data["balance"] = $data->BALANCE ?? 0;
        $obj->data["voucher"] = $data->VOUCHER ?? 0;
        $obj->data["badge"] = $data->BADGE ?? '';
        $obj->data["email"] = $data->EMAIL ?? '';
        $obj->data["firstName"] = $data->FIRSTNAME ?? '';
        $obj->data["middleName"] = $data->MIDDLENAME ?? '';
        $obj->data["lastName"] = $data->LASTNAME ?? '';
        $obj->data["adminActivated"] = $data->ADMINACTIVATED ?? false;
        $obj->data["userActivated"] = $data->USERACTIVATED ?? false;
        $obj->data["isPinSet"] = $data->IS_PIN_SET ?? false;
        $obj->data["qr"] = $data->QR ?? '';
        $obj->data["pinless"] = $data->PINLESS ?? false;
        $obj->data["pinlessLimit"] = $data->PINLESS_LIMIT ?? 0;
        $obj->data["mobile"] = $data->MOBILE ?? '';

        return $obj;
    }

    public static function FillAuthFields() : CustomerAuthFields
    {
        $customerAuthFields = new CustomerAuthFields();
        $customerAuthFields->login = config('paymob-master.paymob-login');
        $customerAuthFields->password = config('paymob-master.paymob-password');
        $customerAuthFields->requestGatewayCode = "W";
        $customerAuthFields->requestGatewayType = "W";
        $customerAuthFields->customerType = config('paymob-master.paymob-customer-type');
        $customerAuthFields->agentCode = config('paymob-master.paymob-agent-code');
        $customerAuthFields->agentPin = config('paymob-master.paymob-agent-pin');

        return $customerAuthFields;
    }

}
