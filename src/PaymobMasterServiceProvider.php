<?php
namespace _34ml\PaymobMaster;
use Illuminate\Support\ServiceProvider;

class PaymobMasterServiceProvider extends ServiceProvider {

    public function boot()
    {
        $this->publishes([
         //   __DIR__.'/Database/migrations/' => database_path('migrations'),
         // __DIR__.'/Models/' => app_path('Models'),
            __DIR__.'/Config/' => config_path(),
        ], 'PaymobMaster');

    }

    public function register()
    {

    }
}
?>
