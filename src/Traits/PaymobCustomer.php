<?php

namespace _34ml\PaymobMaster\Traits;

use _34ml\PaymobMaster\DTOs\Customer\CustomerCreateRequest;
use _34ml\PaymobMaster\DTOs\Customer\CustomerGetRequest;
use _34ml\PaymobMaster\DTOs\Responses\CustomResponse;
use _34ml\PaymobMaster\Helpers\CustomerHelper;
use _34ml\PaymobMaster\Helpers\Helpers;

trait PaymobCustomer
{
  use Authentication;

    public static function CreateCustomer(CustomerCreateRequest $createRequest): CustomResponse
    {
        $authFields = CustomerHelper::FillAuthFields();
        $data = [
            "LOGIN" => $authFields->login,
            "PASSWORD" => $authFields->password,
            "REQUEST_GATEWAY_CODE" => $authFields->requestGatewayCode,
            "REQUEST_GATEWAY_TYPE" => $authFields->requestGatewayType,
            "TYPE" => "CCCREQ",
            "MOBILE" => $createRequest->mobile ?? '',
            "SEND_EMAIL" => $createRequest->sendEmail ?? false,
            "GENERATE_PIN_SEND" => $createRequest->generatePinSend ?? false,
            "QR" => $createRequest->qr ?? $createRequest->email,
            ];

        $hmacData = [
            "AGENTCODE" => $authFields->agentCode,
            "AGENTPIN" => $authFields->agentPin,
            "AGREETERMS" => $createRequest->agreeTerms ?? 1,
            "BADGE" => $createRequest->badge ?? '',
            "CONSUMERTYPE" => $authFields->customerType,
            "EMAIL" => $createRequest->email ?? '',
            "FIRSTNAME" => $createRequest->firstName ?? '',
            "MIDDLENAME" => $createRequest->middleName ?? '',
            "LASTNAME" => $createRequest->lastName ?? '',
            "USERID" => $createRequest->userId ?? ''
        ];

        if(!empty($createRequest->pinlessLimit))
        {
            $hmacData["PINLESS_LIMIT"] = $createRequest->pinlessLimit;
        }
        if(!empty($createRequest->familyId))
        {
            $data["FAMILYID"] = $createRequest->familyId;
        }
        if(!empty($createRequest->familyRole))
        {
            $data["FAMILYROLE"] = $createRequest->familyRole;
        }

        $hmac = Helpers::GenerateHMAC($hmacData);
        $URL = static::$paymobCreateCustomerUrl . "?hmac=" . $hmac;
        $data= array_merge($data, $hmacData);

        $headers = Helpers::$jsonHeader;
        $authorization = "Authorization: Bearer " . $createRequest->token;
        $headers[] = $authorization;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);

        if($response->code != 200) {
           return CustomerHelper::CustomExceptionHandler($response->code);
        }

        return CustomerHelper::ParseCreateResponse($response->jsonResonse);
    }

    public static function GetCustomer(CustomerGetRequest $createRequest): CustomResponse
    {
        $authFields = CustomerHelper::FillAuthFields();
        $data = [
            "LOGIN" => $authFields->login,
            "PASSWORD" => $authFields->password,
            "REQUEST_GATEWAY_CODE" => $authFields->requestGatewayCode,
            "REQUEST_GATEWAY_TYPE" => $authFields->requestGatewayType,
            "TYPE" => "GCCREQ",
        ];

        $hmacData = [
            "AGENTCODE" => $authFields->agentCode,
            "AGENTPIN" => $authFields->agentPin,
            "USERID" => $createRequest->userId ?? ''
        ];

        $hmac = Helpers::GenerateHMAC($hmacData);
        $URL = static::$paymobGetCustomerUrl . "?hmac=" . $hmac;
        $data= array_merge($data, $hmacData);

        $headers = Helpers::$jsonHeader;
        $authorization = "Authorization: Bearer " . $createRequest->token;
        $headers[] = $authorization;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);

        if($response->code != 200) {
            return CustomerHelper::CustomExceptionHandler($response->code);
        }

        return CustomerHelper::ParseGetResponse($response->jsonResonse);
    }
}
