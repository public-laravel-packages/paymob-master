<?php

namespace _34ml\PaymobMaster\DTOs\Wallet;

use _34ml\PaymobMaster\DTOs\PaymobAuthFields;

class SetPinRequest extends PaymobAuthFields
{
    public string $identifier;
    public string $identifierType;
    public string $oldPin;
    public string $newPin;
}
