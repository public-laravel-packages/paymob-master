<?php

namespace _34ml\PaymobMaster\DTOs\Customer;

class CustomerAuthFields
{
    public string $login;
    public string $password;
    public string $requestGatewayCode;
    public string $requestGatewayType;
    public string $customerType;
    public string $agentCode;
    public string $agentPin;
}
