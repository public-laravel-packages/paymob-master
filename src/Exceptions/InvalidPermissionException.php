<?php

namespace _34ml\PaymobMaster\Exceptions;
use Exception;

class InvalidPermissionException extends Exception
{
    public function __construct( $message = "", $code = 0, $previous = null)
    {
        $this->message = !empty($message) ? $message
            : "Invalid Permission";
        parent::__construct($this->message, $code, $previous);
    }
}
