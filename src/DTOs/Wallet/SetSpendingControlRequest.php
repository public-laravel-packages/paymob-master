<?php

namespace _34ml\PaymobMaster\DTOs\Wallet;

use _34ml\PaymobMaster\DTOs\PaymobAuthFields;

class SetSpendingControlRequest extends PaymobAuthFields
{
    public string $rootIdentifier;
    public string $rootIdentifierType;
    public string $consumerPin;
    public string $memberIdentifier;
    public string $memberIdentifierType;
    public string $dailyAmount;
    public string $weeklyAmount;
    public string $monthlyAmount;
}
