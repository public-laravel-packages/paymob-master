<?php

namespace _34ml\PaymobMaster\Traits;

use _34ml\PaymobMaster\DTOs\AcceptPaymob\OrderRegistrationRequest;
use _34ml\PaymobMaster\DTOs\Responses\CustomResponse;
use _34ml\PaymobMaster\Helpers\AcceptHelper;
use _34ml\PaymobMaster\Helpers\Helpers;

trait AcceptPaymob
{
    use Authentication;

    public static function GetPaymentKeys(OrderRegistrationRequest $orderRegistrationRequest): CustomResponse
    {
        $authResponse = static::GetAcceptToken();
        if($authResponse->code == 200)
        {
            $orderRegistrationRequest->token = $authResponse->data["token"];
            $orderResponse = static::OrderRegistration($orderRegistrationRequest);
            if($orderResponse->code == 200)
            {
                return static::PaymentKeys($orderRegistrationRequest, $orderResponse->data["id"]);
            }
            else
            {
                return $orderResponse;
            }
        }
        else
        {
            return $authResponse;
        }
    }
    public static function GetAcceptToken(): CustomResponse
    {
        static::GetConfig();

        $data = [
            "api_key" =>  static::$paymobAcceptAuthAPIKey,
            ];

        $URL = static::$paymobAcceptAuthUrl;
        $headers = Helpers::$jsonHeader;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);

        if($response->code != 201) {
            return AcceptHelper::ExceptionHandler($response->code);
        }

        return AcceptHelper::ParseAuthResponse($response->jsonResonse);
    }
    public static function OrderRegistration(OrderRegistrationRequest $orderRegistrationRequest): CustomResponse
    {
        static::GetConfig();

        $data = [
            "auth_token" =>  $orderRegistrationRequest->token,
            "delivery_needed" => $orderRegistrationRequest->deliveryNeeded,
            "amount_cents" => (int) $orderRegistrationRequest->amount * 100,
            "currency" => $orderRegistrationRequest->currency,
            "merchant_order_id" => $orderRegistrationRequest->merchantOrderId,
            "items" => $orderRegistrationRequest->items,
            "shipping_data" => $orderRegistrationRequest->shippingData ?: [],
            "shipping_details" => $orderRegistrationRequest->shippingDetails ?: []
        ];

        $URL = static::$paymobOrderRegistrationUrl;
        $headers = Helpers::$jsonHeader;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);
        if($response->code != 201 && $response->code != 200) {
           return AcceptHelper::ExceptionHandler($response->code);
        }

        return AcceptHelper::ParseOrderRegistration($response->jsonResonse);
    }
    public static function PaymentKeys(OrderRegistrationRequest $orderRegistrationRequest, $orderId): CustomResponse
    {
        static::GetConfig();

        $data = [
            "auth_token" =>  $orderRegistrationRequest->token,
            "amount_cents" => (int) $orderRegistrationRequest->amount * 100,
            "currency" => $orderRegistrationRequest->currency,
            "expiration" => 3600,
            "order_id" => $orderId,
            "integration_id" => static::$paymobMotoIntegrationId,
            "lock_order_when_paid" => "false",
            "billing_data" => [
                "apartment" => "NA",
                "email" => $orderRegistrationRequest->shippingData["email"],
                "floor" => "NA",
                "first_name" => $orderRegistrationRequest->shippingData["first_name"],
                "street" => "NA",
                "building" => "NA",
                "phone_number" => $orderRegistrationRequest->shippingData["phone_number"],
                "shipping_method" => "NA",
                "postal_code" => "NA",
                "city" => "NA",
                "country" => "NA",
                "last_name" => $orderRegistrationRequest->shippingData["last_name"],
                "state" => "NA"
            ]
        ];

        $URL = static::$paymobPaymentKeysUrl;
        $headers = Helpers::$jsonHeader;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);
        if($response->code != 201 && $response->code != 200) {
            return AcceptHelper::ExceptionHandler($response->code);
        }

        return AcceptHelper::ParsePaymentKeys($response->jsonResonse, $orderId);
    }


}
