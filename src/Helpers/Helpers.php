<?php

namespace _34ml\PaymobMaster\Helpers;

use _34ml\PaymobMaster\DTOs\PaymobResponse;

class Helpers
{
    public static array $jsonHeader = [
            'Content-Type: application/json'
        ];

    public static array $wwwFormUrlEncodedHeader = [
        "cache-control: no-cache",
        'Content-Type: application/x-www-form-urlencoded'
    ];
    public static function SendRequest(string $url, string $data, array $headers = null) : PaymobResponse
    {
        if(empty($headers))
            $headers = static::$jsonHeader;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $curlResponse = curl_exec($ch);
        curl_close($ch);

        $info = curl_getinfo($ch);
        return new PaymobResponse($curlResponse,$info["http_code"]);
    }


    public static function GenerateHMAC(array $data)
    {
        $hmac_secret = config('paymob-master.paymob-hmac-secret');
        $hmac_data = array_map(function ($v) {

            switch (gettype($v)) {
                case 'boolean':
                    return $v ? 'true' : 'false';

                default:
                    return $v;
            }

        }, $data);

        $hmac_string = implode('', $hmac_data);

        return hash_hmac('sha512', $hmac_string, $hmac_secret);
    }

}
