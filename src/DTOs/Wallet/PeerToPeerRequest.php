<?php

namespace _34ml\PaymobMaster\DTOs\Wallet;

use _34ml\PaymobMaster\DTOs\PaymobAuthFields;

class PeerToPeerRequest extends PaymobAuthFields
{
    public string $rootIdentifier;
    public string $rootIdentifierType;
    public string $memberIdentifier;
    public string $memberIdentifierType;
    public string $amount;

}
