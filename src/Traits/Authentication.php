<?php

namespace _34ml\PaymobMaster\Traits;

use _34ml\PaymobMaster\DTOs\Responses\CustomResponse;
use _34ml\PaymobMaster\Helpers\CustomerHelper;
use _34ml\PaymobMaster\Helpers\Helpers;

trait Authentication
{
    protected static $paymobCustomerAuthUrl;
    protected static $paymobCreateCustomerUrl;
    protected static $paymobGetCustomerUrl;
    protected static $paymobSetSpendingLimitUrl;
    protected static $paymobAcceptAuthUrl;
    protected static $paymobOrderRegistrationUrl;
    protected static $paymobPaymentKeysUrl;


    protected static $paymobCustomerUsername;
    protected static $paymobCustomerPassword;
    protected static $paymobCustomerClientId;
    protected static $paymobCustomerClientSecret;

    protected static $paymobAcceptAuthAPIKey;
    protected static $paymobMotoIntegrationId;

    protected static function GetConfig()
    {
        static::$paymobCustomerAuthUrl = config('paymob-master.paymob-customer-auth-url');
        static::$paymobCreateCustomerUrl = config('paymob-master.paymob-customer-create-url');
        static::$paymobGetCustomerUrl = config('paymob-master.paymob-customer-get-url');
        static::$paymobSetSpendingLimitUrl = config('paymob-master.paymob-wallet-set-spending-limit');
        static::$paymobAcceptAuthUrl = config('paymob-master.paymob-accept-auth-url');
        static::$paymobOrderRegistrationUrl = config('paymob-master.paymob-order-registration-url');
        static::$paymobPaymentKeysUrl = config('paymob-master.paymob-payment-keys-url');

        static::$paymobCustomerUsername = config('paymob-master.paymob-customer-username');
        static::$paymobCustomerPassword = config('paymob-master.paymob-customer-password');
        static::$paymobCustomerClientId = config('paymob-master.paymob-customer-clientId');
        static::$paymobCustomerClientSecret = config('paymob-master.paymob-customer-clientSecret');

        static::$paymobAcceptAuthAPIKey = config('paymob-master.paymob-accept-api-key');
        static::$paymobMotoIntegrationId = config('paymob-master.paymob-moto-integration-id');
    }

    public static function Authenticate() : CustomResponse
    {
        static::GetConfig();
        $data = "grant_type=" . "password" .
            "&username=" . static::$paymobCustomerUsername .
            "&password=" . static::$paymobCustomerPassword .
            "&client_id=" . static::$paymobCustomerClientId .
            "&client_secret=" . static::$paymobCustomerClientSecret;

        $response = Helpers::SendRequest(static::$paymobCustomerAuthUrl, $data, Helpers::$wwwFormUrlEncodedHeader);
        if($response->code != 200) {
          return CustomerHelper::CustomExceptionHandler($response->code);
        }

        return CustomerHelper::ParseAuthResponse($response->jsonResonse);
    }

}
