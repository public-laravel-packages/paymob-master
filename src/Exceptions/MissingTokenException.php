<?php

namespace _34ml\PaymobMaster\Exceptions;
use Exception;

class MissingTokenException extends Exception
{
    public function __construct( $message = "", $code = 0, $previous = null)
    {
        $this->message = !empty($message) ? $message
            : "Missing Token";
        parent::__construct($this->message, $code, $previous);
    }
}
