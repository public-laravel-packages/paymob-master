<?php

namespace _34ml\PaymobMaster\Exceptions;
use Exception;

class UnhandledException extends Exception
{
    public function __construct( $message = "", $code = 0, $previous = null)
    {
        $this->message = !empty($message) ? $message
            : "Unknown Exception";
        parent::__construct($this->message, $code, $previous);
    }
}
