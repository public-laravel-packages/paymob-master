<?php

namespace _34ml\PaymobMaster\Traits;

use _34ml\PaymobMaster\DTOs\Responses\CustomResponse;
use _34ml\PaymobMaster\DTOs\Wallet\PeerToPeerRequest;
use _34ml\PaymobMaster\DTOs\Wallet\SetPinRequest;
use _34ml\PaymobMaster\DTOs\Wallet\SetSpendingControlRequest;
use _34ml\PaymobMaster\Helpers\CustomerHelper;
use _34ml\PaymobMaster\Helpers\Helpers;

trait PaymobWallet
{
  use Authentication;

    public static function SetSpendingLimit(SetSpendingControlRequest $createRequest): CustomResponse
    {
        $authFields = CustomerHelper::FillAuthFields();
        $data = [
            "LOGIN" => $authFields->login,
            "PASSWORD" => $authFields->password,
            "REQUEST_GATEWAY_CODE" => $authFields->requestGatewayCode,
            "REQUEST_GATEWAY_TYPE" => $authFields->requestGatewayType,
            "MSISDN" => $authFields->agentCode,
            "PIN" => $authFields->agentPin,
            "TYPE" => "ECSCREQ",
            "MERCODES" => [],
            "AGENTCODE" => $authFields->agentCode,
            "AGENTPIN" => $authFields->agentPin,
            "CHILD_IDENTIFIER" => $createRequest->memberIdentifier,
            "CHILD_IDENTIFIER_TYPE" => $createRequest->memberIdentifierType,
            "CONSUMERPIN" => $createRequest->consumerPin ?: "",
            "DAILYAMOUNT" => sprintf("%.1f", $createRequest->dailyAmount),
            "IDENTIFIER" => $createRequest->rootIdentifier,
            "IDENTIFIER_TYPE" => $createRequest->rootIdentifierType,
            "MONTHLYAMOUNT" => sprintf("%.1f", $createRequest->monthlyAmount),
            "WEEKLYAMOUNT" => sprintf("%.1f", $createRequest->weeklyAmount),
            ];

        $hmacData = [
            "AGENTCODE" => $authFields->agentCode,
            "AGENTPIN" => $authFields->agentPin,
            "CHILD_IDENTIFIER" => $createRequest->memberIdentifier,
            "CHILD_IDENTIFIER_TYPE" => $createRequest->memberIdentifierType,
            "CONSUMERPIN" => $createRequest->consumerPin,
            "DAILYAMOUNT" => sprintf("%.1f", $createRequest->dailyAmount),
            "IDENTIFIER" => $createRequest->rootIdentifier,
            "IDENTIFIER_TYPE" => $createRequest->rootIdentifierType,
            "MONTHLYAMOUNT" => sprintf("%.1f", $createRequest->monthlyAmount),
            "WEEKLYAMOUNT" => sprintf("%.1f", $createRequest->weeklyAmount),
        ];


        $hmac = Helpers::GenerateHMAC($hmacData);
        $URL = static::$paymobSetSpendingLimitUrl . "?hmac=" . $hmac;

        $headers = Helpers::$jsonHeader;
        $authorization = "Authorization: Bearer " . $createRequest->token;
        $headers[] = $authorization;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);

        if($response->code != 200) {
           return CustomerHelper::CustomExceptionHandler($response->code);
        }

        return CustomerHelper::ParseCreateResponse($response->jsonResonse);
    }

    public static function PeerToPeer(PeerToPeerRequest $createRequest): CustomResponse
    {
        $authFields = CustomerHelper::FillAuthFields();
        $data = [
            "LOGIN" => $authFields->login,
            "PASSWORD" => $authFields->password,
            "REQUEST_GATEWAY_CODE" => $authFields->requestGatewayCode,
            "REQUEST_GATEWAY_TYPE" => $authFields->requestGatewayType,
            "TYPE" => "EP2PREQ",
            "IDENTIFIER" => $createRequest->rootIdentifier,
            "IDENTIFIER_TYPE" => $createRequest->rootIdentifierType,
            "CONSUMERPIN" => "",
            "IDENTIFIER2" => $createRequest->memberIdentifier,
            "IDENTIFIER2_TYPE" => $createRequest->memberIdentifierType,
            "MSISDN" => $authFields->agentCode,
            "PIN" => $authFields->agentPin,
            "AMOUNT" => sprintf("%.1f", $createRequest->amount),
        ];

        $hmacData = [
            "AMOUNT" => sprintf("%.1f", $createRequest->amount),
            "CONSUMERPIN" => "",
            "IDENTIFIER" => $createRequest->rootIdentifier,
            "IDENTIFIER2" => $createRequest->memberIdentifier,
            "IDENTIFEIR_TYPE" => $createRequest->rootIdentifierType,
            "IDENTIFIER2_TYPE" => $createRequest->memberIdentifierType,
            "MSISDN" => $authFields->agentCode,
            "PIN" => $authFields->agentPin,
        ];

        $hmac = Helpers::GenerateHMAC($hmacData);
        $URL = static::$paymobSetSpendingLimitUrl . "?hmac=" . $hmac;


        $headers = Helpers::$jsonHeader;
        $authorization = "Authorization: Bearer " . $createRequest->token;
        $headers[] = $authorization;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);

        if($response->code != 200) {
            return CustomerHelper::CustomExceptionHandler($response->code);
        }

        return CustomerHelper::ParseCreateResponse($response->jsonResonse);
    }

    public static function SetOrChangePin(SetPinRequest $createRequest): CustomResponse
    {
        $authFields = CustomerHelper::FillAuthFields();
        $data = [
            "LOGIN" => $authFields->login,
            "PASSWORD" => $authFields->password,
            "REQUEST_GATEWAY_CODE" => $authFields->requestGatewayCode,
            "REQUEST_GATEWAY_TYPE" => $authFields->requestGatewayType,
            "TYPE" => "ECPREQ",
            "IDENTIFIER" => $createRequest->identifier,
            "IDENTIFIER_TYPE" => $createRequest->identifierType,
            "MSISDN" => $authFields->agentCode,
            "PIN" => $authFields->agentPin,
            "OLDPIN" => $createRequest->oldPin,
            "NEWPIN" => $createRequest->newPin,
        ];

        $hmacData = [
            "IDENTIFIER" => $createRequest->identifier,
            "IDENTIFIER_TYPE" => $createRequest->identifierType,
            "MSISDN" => $authFields->agentCode,
            "NEWPIN" => $createRequest->newPin,
            "OLDPIN" => "",
            "PIN" => $authFields->agentPin,
        ];

        $hmac = Helpers::GenerateHMAC($hmacData);
        $URL = static::$paymobSetSpendingLimitUrl . "?hmac=" . $hmac;


        $headers = Helpers::$jsonHeader;
        $authorization = "Authorization: Bearer " . $createRequest->token;
        $headers[] = $authorization;

        $response = Helpers::SendRequest($URL, json_encode($data),$headers);

        if($response->code != 200) {
            return CustomerHelper::CustomExceptionHandler($response->code);
        }

        return CustomerHelper::ParseCreateResponse($response->jsonResonse);
    }
}
