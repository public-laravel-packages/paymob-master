<?php
// config for _34ML/PaymobMaster

return [
    "paymob-customer-auth-url" => env('PAYMOB_CUSTOMER_AUTH_URL', 'null'),
    "paymob-customer-username" => env('PAYMOB_CUSTOMER_USERNAME', 'null'),
    "paymob-customer-password" => env('PAYMOB_CUSTOMER_PASSWORD', 'null'),
    "paymob-customer-clientId" => env('PAYMOB_CUSTOMER_CLIENT_ID', 'null'),
    "paymob-customer-clientSecret" => env('PAYMOB_CUSTOMER_CLIENT_SECRET', 'null'),

    ];

?>
