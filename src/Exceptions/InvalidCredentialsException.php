<?php

namespace _34ml\PaymobMaster\Exceptions;
use Exception;

class InvalidCredentialsException extends Exception
{
    public function __construct( $message = "", $code = 0, $previous = null)
    {
        $this->message = !empty($message) ? $message
            : "Invalid Credentials";
        parent::__construct($this->message, $code, $previous);
    }
}
