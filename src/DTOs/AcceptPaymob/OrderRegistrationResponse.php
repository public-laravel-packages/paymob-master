<?php

namespace _34ml\PaymobMaster\DTOs\AcceptPaymob;

use _34ml\PaymobMaster\DTOs\PaymobAuthFields;

class OrderRegistrationResponse
{
    public int $code;
    public string $id;
}
