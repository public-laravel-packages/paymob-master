<?php

namespace _34ml\PaymobMaster\DTOs\AcceptPaymob;

use _34ml\PaymobMaster\DTOs\PaymobAuthFields;

class OrderRegistrationRequest  extends PaymobAuthFields
{
    public bool $deliveryNeeded;
    public float $amount;
    public string $currency;
    public string $merchantOrderId;
    public array $items;
    public array $shippingData;
    public array $shippingDetails;
}
