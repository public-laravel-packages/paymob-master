<?php

namespace _34ml\PaymobMaster\Helpers;

use _34ml\PaymobMaster\DTOs\Responses\CustomResponse;

class AcceptHelper
{
    public static function ExceptionHandler($code): CustomResponse
    {
        switch ($code) {
            case 1:
                return new CustomResponse($code,"Empty Response");

            case 400:
                return new CustomResponse($code,"Un expected error");

            case 401:
                return new CustomResponse($code,"Integration Error");

            case 500:
                return new CustomResponse($code,"Un expected error");

            default:
                return new CustomResponse($code,"Un expected error");
        }
    }

    public static function ParseAuthResponse(string $json) : CustomResponse
    {
       $data = json_decode($json);

       if(empty($data))
        return AcceptHelper::ExceptionHandler(1);

       $obj = new CustomResponse();
       $obj->code = 200;
       $obj->data["token"] = $data->token ?? "";
       return $obj;
    }

    public static function ParseOrderRegistration(string $json) : CustomResponse
    {
        $data = json_decode($json);

        if(empty($data))
            return AcceptHelper::ExceptionHandler(1);

        $obj = new CustomResponse();
        $obj->code = 200;
        $obj->data["id"] = $data->id;
        return $obj;
    }


    public static function ParsePaymentKeys(string $json, string $orderId) : CustomResponse
    {
        $data = json_decode($json);

        if(empty($data))
            return AcceptHelper::ExceptionHandler(1);

        $obj = new CustomResponse();
        $obj->code = 200;
        $obj->data["token"] = $data->token;
        $obj->data["order_id"] = $orderId;
        return $obj;
    }

}
