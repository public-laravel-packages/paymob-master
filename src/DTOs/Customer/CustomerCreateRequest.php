<?php

namespace _34ml\PaymobMaster\DTOs\Customer;

use _34ml\PaymobMaster\DTOs\PaymobAuthFields;

class CustomerCreateRequest extends PaymobAuthFields
{
    public int $agreeTerms;
    public bool $sendEmail;
    public string $badge;
    public string $userId;
    public string $email;
    public string $firstName;
    public string $middleName;
    public string $lastName;
    public string $mobile;
    public string $familyId;
    public string $familyRole;
    public bool $generatePinSend;
    public string $qr;
    public Float $pinlessLimit;
}
