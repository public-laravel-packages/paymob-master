<?php

namespace _34ml\PaymobMaster\DTOs;

class PaymobResponse
{
   public string $jsonResonse;
   public int $code;

    public function __construct(
        string $jsonResonse,
        int $code
    )
    {
        $this->jsonResonse = $jsonResonse;
        $this->code = $code;
    }
}
