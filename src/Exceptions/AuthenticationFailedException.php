<?php

namespace _34ml\PaymobMaster\Exceptions;
use Exception;

class AuthenticationFailedException extends Exception
{
    public function __construct( $message = "", $code = 0, $previous = null)
    {
        $this->message = !empty($message) ? $message
            : "Authentication Failed";
        parent::__construct($this->message, $code, $previous);
    }
}
